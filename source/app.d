import std.stdio;
import dsfml.graphics;

import global;
import poolTable;

void handleEvent(Event e) {
	if(e.type == Event.EventType.MouseButtonReleased) {
		table.cue.shoot = true;
	}
}

void main()
{
	dsfmlInitWindow(window);

	table = new PoolTable();

	while(window.isOpen()) {
		Event event;
		while (window.pollEvent(event)){
			handleEvent(event);
			// "close requested" event: we close the window
			if (event.type == Event.EventType.Closed){
					window.close();
				}
		}
		window.clear();
		table.run();
		table.display();
		window.display();
	}
}
