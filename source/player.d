module player;

import global;
import neuralnet;
import poolTable;

class Player {
  int score;
  Player op; //Oponent
  Network net;
  Ball*[] balls;
  /**
  Network dimensions:
  * Inputs:
    * Position of all balls (relative)
    * Angle to each ball
    * Angle to each pocket
  * Outputs:
    * Cue angle
    * Cue velocity
  */

  float _fitness;

  void setOp(ref Player o) {
    op = o;
  }

  this(ref Ball*[]) {
    net = new Network(NE_DIMENSIONS);
  }

  void haveTurn() {
    
  }

  @property fitness() {
    fitness = score-op.score*0.3;
    return fitness;
  }
}
