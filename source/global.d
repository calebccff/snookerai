
public import utils;
import dsfml.graphics;

///Handle to the window for drawing graphics
RenderWindow window;

///Quick reference to window dimensions
float
  width = 1300,
  height = 800,
  scale = 2; //pixels per ft

int[] NE_DIMENSIONS = [69, 50, 10, 2];

const
  B_D = 8,//5.25, //Ball diameter
  P_D = 12;//8; //Pocket diameter


import poolTable;
PoolTable table;

public import std.random;
Random rnd;

static this() {
  rnd = Random(unpredictableSeed);
}