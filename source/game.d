module game;

import std.stdio;
import dsfml.graphics;
import dsfml.system;

import global;
import player;
import poolTable;

class Game {
  PoolTable table;
  Player[] players;
  int which = 0;

  void tick() { //One game "tick" (one player turn)
    players[which].haveTurn();


    which = (++which)%2;
  }

}