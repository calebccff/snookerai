module poolTable;

import std.stdio;
import std.math;
import std.conv;

import global;
import dsfml.graphics;
import dsfml.system;

///The pool table itself
class PoolTable
{
  Ball[15] reds;
  Ball[6] colors;
  Ball qball;
  Ball*[] balls;

  Cue cue;

  Vector2f pos;
  Vector2f dim;
  
  RenderStates st;

  this() {
    dim = Vector2f(178*scale, 357*scale);
    pos = Vector2f(width/2, height/2);

    foreach(long i, ref r; reds) {
      const row = to!int((-1+sqrt(cast(float)(1+8*i)))/2f)-1; //The current row in the rack
      const rc = i-(row*(row+1))/2-1; //The difference between current ball and first ball on this row
      const x = -row*B_D*scale*1.5+rc*B_D*scale;
      r = new Ball(red, Vector2f(x-B_D*scale/2, dim.y*1/3+row*B_D*scale+B_D*scale));
      balls ~= &r;
    }
    colors[0] = new Ball(yello, Vector2f(-dim.x/3, -dim.y/3));
    colors[1] = new Ball(green, Vector2f(dim.x/3 , -dim.y/3));
    colors[2] = new Ball(brown, Vector2f(0       , -dim.y/3));
    colors[3] = new Ball(blue , Vector2f(0       , 0));
    colors[4] = new Ball(pink , Vector2f(0       , dim.y/3-B_D*scale*2));
    colors[5] = new Ball(black, Vector2f(0       , dim.y/3+dim.y/8));
    foreach(ref c; colors) {
      balls ~= &c;
    }
    qball     = new Ball(white, Vector2f(-dim.x/3+dim.x/6, -dim.y/3));
    balls ~= &qball;

    cue = new Cue(&qball);

  }

  void run() {
    Transform t;
    t.translate(pos.x, pos.y);
    st.transform = t;
    foreach(ref ball; balls) {
      ball.run();
      foreach(ref ob; balls) {
        if(ob !is ball) {
          ball.collide(*ob);
        }
      }
    }
    cue.run();
  }

  void display() {
    RectangleShape s = new RectangleShape(dim);
    //s.origin = dim/2;
    
    s.position = -dim/2;
    s.fillColor = Color.Green;
    s.outlineColor = Color.Magenta;
    s.outlineThickness = 22;
    
    window.draw(s, st);
    
    CircleShape p = new CircleShape(P_D*scale/2);
    p.fillColor = Color.Red;
    p.origin = Vector2f(10, 10);
    foreach(i, ppos; [
    Vector2f(s.size.x/2+2, s.size.y/2+2),
    Vector2f(s.size.x/2+2, -2),
    Vector2f(s.size.x/2+2, -s.size.y/2-2),
    Vector2f(-s.size.x/2-2, -s.size.y/2-2),
    Vector2f(-s.size.x/2+2, -2),
    Vector2f(-s.size.x/2-2, s.size.y/2+2),
    ]) {
      p.position = ppos;
      window.draw(p, st);
      foreach(ref b; balls) {
        if(*b !is qball) {
          Vector2f diff = b.pos-ppos;
          if(diff.mag < P_D*scale/2+B_D*scale/2) {
            b.potted = true;
          }
        }
      }
    }

    foreach(ref b; balls) {
      b.display(st);
    }
    qball.display(st);
    cue.display();
  }
}

class Ball {
  BallType type;
  Vector2f pos;
  Vector2f vel;
  bool potted = false;

  this(BallType bt=red, Vector2f _pos=Vector2f(0, 0)) {
    type = bt;
    pos = _pos;
    writeln(pos);
    vel = Vector2f(0, 0);
  }

  void run() {
    if(potted) return;
    pos += vel;
    if(pos.x > table.dim.x/2-B_D/2*scale || pos.x < -table.dim.x/2+B_D/2*scale) {
      vel.x *= -1;
    }
    if(pos.y > table.dim.y/2-B_D/2*scale || pos.y < -table.dim.y/2+B_D/2*scale) {
      vel.y *= -1;
    }
    constrain(pos, -table.dim.x/2+B_D/2*scale, table.dim.x/2-B_D/2*scale, -table.dim.y/2+B_D/2*scale, table.dim.y/2-B_D/2*scale);
    vel.mag = vel.mag-(9.81/scale*0.01)*(2-2/(vel.mag+1));
    if(vel.mag < 0.05) vel.mag = 0; //Totally accurate phsyics(TM)
  }

  ///Calculates the velocities after a collision, docs (here)[https://ericleong.me/research/circle-circle/#dynamic-static-circle-collision-response]
  void collide(ref Ball ob) {
    if(potted) return;
    const Vector2f dist = ob.pos-pos;
    if(dist.mag > B_D*scale) {
      return;
    }
    if(dist.mag < B_D*scale) {
      Vector2f off = dist;
      off.mag = B_D*scale-dist.mag;
      pos -= off/2;
      ob.pos += off/2;
    }
    Vector2f norm = dist/dist.mag;
    float p = (2*(vel.dot(norm)-ob.vel.dot(norm)))/(1+1); //Divide by total mass
    Vector2f n1 = norm*p*1; //Mass of ball
    Vector2f n2 = norm*p*1; //Mass of other ball
    Vector2f w1 = vel-n1;
    Vector2f w2 = ob.vel+n2;
    vel = w1*0.98;
    ob.vel = w2*0.95;
  }

  void display(RenderStates st) {
    if(potted) return;
    CircleShape b = new CircleShape(B_D*scale/2);
    b.fillColor = type;
    b.position = pos;
    b.origin = Vector2f(B_D*scale/2, B_D*scale/2);
    window.draw(b, st);
  }
}

class Cue {
  Vector2f pos;
  Ball* qball;
  float ang;
  RectangleShape s;
  RenderStates st;
  bool shoot = false;
  float _charge = 0;

  @property float charge(float v){
    if(v < 15) {
      _charge = v;
    }
    return _charge;
  }
  @property float charge(){
    return _charge;
  }

  this(Ball *q)
  {
    qball = q;
    s = new RectangleShape(Vector2f(150*scale, 2*scale));
    s.fillColor = Color.White;
    s.origin = Vector2f(-10*scale, scale);
  }

  void run()
  {

    if(Mouse.isButtonPressed(Mouse.Button.Left)) {
      charge = charge+1f/3f;
    }

    Transform t;
    Vector2f diff = qball.pos+table.pos-(Mouse.getPosition(window));
    t.translate(table.pos.x+qball.pos.x, table.pos.y+qball.pos.y);
    float ang = atan2(diff.x, diff.y)/PI*180;
    t.rotate(-90-ang);
    st.transform = t;

    s.origin = Vector2f(-(charge+5)*scale, scale);
    writeln(qball.vel.mag);
    if(shoot && qball.vel.mag.approxEqual(0, 0.05, 0.05)){
      shoot = false;
      qball.vel = Vector2f(0, -1);
      qball.vel.rotate(ang);
      qball.vel *= charge;
      charge = 0;
    }
  }
  void display()
  {
    if(qball.vel.mag == 0) {
      window.draw(s, st);
      s.origin = Vector2f(s.size.x+20, scale);
      window.draw(s, st);
    }
  }
}